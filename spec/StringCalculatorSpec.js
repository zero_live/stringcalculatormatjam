describe('stringCalculator', () => {
  it('does not sum for an empty string', () => {
    //arrange
    const emptyString = ""
    const valueForEmptyString = 0

    //act
    const result = stringCalculator(emptyString)

    //assert
    expect(result).toEqual(valueForEmptyString)
  })

  it('sums one value', () => {

    const result = stringCalculator("1")

    expect(result).toEqual(1)
  })

  it('sums values separated by coma', () => {

    const result = stringCalculator("1,2")

    expect(result).toEqual(3)
  })
})

function stringCalculator(rawValues) {
  const values = new Values(rawValues)
  const calculator = new Calculator(values)

  return calculator.sum()
}

class Calculator {
  constructor(values) {
    this.values = values
  }

  sum() {
    const valueForEmptyString = 0
    if (this.values.isEmpty()) { return valueForEmptyString }

    const sum = this.values.toIntegers().reduce(this.sumValues)

    return sum
  }

  sumValues(acc, value) {
    return acc + value
  }
}

class Values {
  constructor(values) {
    this.values = values
  }

  toIntegers() {
    const separator = ','
    const splittedValues = this.values.split(separator)
    const asIntegers = splittedValues.map(this.toInteger)

    return asIntegers
  }

  toInteger(value) {
    return parseInt(value)
  }

  isEmpty() {
    return this.values === ""
  }
}
